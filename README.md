# IDS721 Week3 Project - Peter Liu

## I accidentally put this in the group where I only have "Developer" access. The Gitlab stores the screenshots and the CodeCatalyst stores the code functionality for this project.
Please use my Code Catalyst Link to see the code:  https://codecatalyst.aws/spaces/project3-Peter-Liu/projects/project3-peter-liu/view

Please email pl217@duke.edu if there is trouble accessing this link. 
 
## The Remaining Documentation has screenshots, and is the same as the CodeCatalyst Site. 

## S3 Bucket Screenshots (pl217, Peter Liu)
* My (Peter Liu's) S3 bucket is up and running
![S3_Bucket_List](/upandrunning.png)
* The bucket has versioning enabled
![S3_Versioning](/versioningenabled.png)
* The bucket has encryption enabled
![S3_Encryption](/encryptionenabled.png)
* This project was done with Cloud9, and includes CodeWhisperer's usage.
![Cloud9](/awscloud9.png)

## Key Steps
1. Follow CDK Installation first.
2. Sign up for Codecatalyst and create an empty AWS Cloud9 env. 
3. During the CodeCatalyst signup, create a new project. Use the template for "Creating a Customized Coding Companion with Amazon CodeWhisperer on AWS" during the creation. This will initialize a repo with most of the following code included.
4. Go AWS Portal and create a new user with permissions:
   * `AmazonS3FullAccess`
   * `AWSLambda_FullAccess`
   * `IAMFullAccess`
   * `AmazonEC2ReadOnlyAccess`
  and two with the following permissions
   * `CloudFormation`
   * `Systems Manager`
5. Store AWS Key and AWS Secret Key.
6. Navigate back to the Cloud9 environment and run `aws configure` in the terminal
7. Enter the access key and secret access key
8. Run npm install to install appropriate packages.
9. Then use AWS CodeWhisperer to create an S3 bucket with versioning control at /customized-coding-companion/lib/s3Resources.construct.ts.
10. 9. Run cdk deploy. Follow the steps further below to ensure everythng is working properly.
11. Use AWS CodeWhisperer to alter the lib\lambda\nodejs\table_loader\index_ts to debug the typing error.
12. Repeat cdk deploy. It should work.

## AWS CodeWhisperer
* AWS CodeWhisperer, an AWS service tool to generate code, was used in this project. 
* The following prompts were used to generate and alter code for S3 Bucket.
* In file: /customized-coding-companion/lib/s3Resources.construct.ts, 
* I used AWS CodeWhisperer with the prompts: "// Creates an S3 bucket with enforced SSL, block public access, and S3 managed encryption. The bucket is also configured with versioning to keep a history of object changes and has a removal policy based on the context settings. Server access logging is enabled with a designated prefix."
* As well as the prompt "        // Integrate version control "
* I also used AWS CodeWhisperer to debug the TypeScript object type error that popped up in lib\lambda\nodejs\table_loader\index_ts.


## See this Youtube Tutorial for More Information
* [AWS CDK Crash course](https://www.youtube.com/watch?v=T-H4nJQyMig)

# Overall Code Description: Creating a Customized Coding Companion with Amazon CodeWhisperer on AWS

## Table of Contents

1. [Overview](#overview)
2. [Deployment Steps](#deployment-steps)
3. [Deployment Validation](#deployment-validation)
4. [Running the Guidance](#running-the-guidance)
5. [Next Steps](#next-steps)
6. [Cleanup](#cleanup)

## Overview

This code enables users to "train" their own Amazon CodeWhisperer. This code is primarily (90%) cloned from an Amazon CodeWhisperer Professional template, and then refined using AWS CodeWhisperer. By detecting, downloading, and preparing new repository releases, users can deploy their own CodeWhisperer instance.

## Deployment Steps

### Option 1: Deploy via Amazon CodeCatalyst blueprint

1. Create a new project in Amazon CodeCatalyst
2. Select **Start with a blueprint** and choose the **Custom AI Code Generator** blueprint. Click **Next**.
3. Give your project a name.
4. Select an **AWS account**, **IAM Role**, and **AWS Region**. Optionally configure settings under **Additional configuration options**.
5. Click **Create project**.

### Option 2: Manually deploy CDK application
1. Clone this repository and cd into it.
3. Install required packages in using command `npm install`
4. Edit the following attribute values in **cdk.json**:

| Attribute value           | Description                                                                                                                               |
| ------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------- |
| `public_github_repos`     | array of the repositories you'd like to include in Amazon CodeWhisperer customizations. The string format is `GITHUB_ORG/REPOSITORY_NAME` |
| `bucket_name_prefix`      | The prefix of the Amazon S3 bucket name to be created                                                                                     |
| `bucket_removal_policy`   | The removal policy of the Amazon S3 bucket name to be created. Should be `DESTROY` or `RETAIN``.                                          |
| `stack_name`              | The name of the AWS CloudFormation Stack to be created                                                                                    |
| `update_interval_minutes` | The interval (minutes) in which this process will run on a recurring basis                                                                |
5. Do cdk deploy.

## Running the Guidance

### Observe repository table in Amazon DynamoDB

1. From the [AWS Management Console](https://console.aws.amazon.com) in your web browser, open the Amazon DynamoDB console and click **Tables** on the
   left-hand menu.

2. Select the table starting with the prefix `CustomAiCodeGenerator`, and click **Explore table items**.

3. Observe the repositories you have configured for downloading/extraction into Amazon S3. Attributes include `id`, `enabled`, `ignore_file_s3_url`,
   `modified`, and `version`.


### Observe Amazon S3 bucket contents

1. From the [AWS Management Console](https://console.aws.amazon.com) in your web browser, open the Amazon S3 console.
2. Click the S3 bucket starting with the name observed from the previous step.
3. Observe the contents of the bucket. The latest versions of configured repositories are stored in the `current/` prefix. Older versions that have
   been replaced are relocated under the `archived/` prefix.

### Create an Amazon CodeWhisperer customization

1. From the [AWS Management Console](https://console.aws.amazon.com) in your web browser, open the Amazon CodeWhisperer console, and click
   'Customizations' in the left-hand menu.



2. Click **Create customization**

![Customizations](assets/images/customizations.png)

3. Enter a Customization name and description.
4. For Source Provider, select **Amazon S3**, and enter the following S3 URI using the bucket name from the previous steps:
   `s3://<BUCKET_NAME>/current/`
5. Click **Create customization**


## Next Steps

### Use your customization

Learn how to use your new customization in the [Amazon CodeWhisperer User Guide](https://docs.aws.amazon.com/codewhisperer/latest/userguide/customizations.html).

### Adding additional repositories

1. To include additional repositories for downloading/extraction into Amazon S3, add new strings to the `public_github_repos` array attribute in
   **cdk.json**.
2. Re-deploy the CDK application (manually with `cdk deploy`, or pushing a new commit to your CodeCatalyst repository, which will trigger a
   deployment).

### Removing/disabling repositories

1. To include additional repositories for downloading/extraction into Amazon S3, remove the repository strings(s) from the `public_github_repos` array
   attribute in **cdk.json**.
2. Re-deploy the CDK application (manually with `cdk deploy`, or pushing a new commit to your CodeCatalyst repository, which will trigger a
   deployment).

## Cleanup

To delete and cleanup deployed resources, use one of the following methods:

- To cleanup with AWS CDK:
  - If not currently in the `source` directory, run the command `cd source`
  - Run the command `cdk destroy`
  - Enter `y` when prompted `Are you sure you want to delete: CustomAiCodeGeneratorStack (y/n)?`.
- From the [AWS Management Console](https://console.aws.amazon.com) in your web browser, open the CloudFormation console, click **Stacks** on the
  left-hand menu, select the stack with the name **CustomAiCodeGeneratorStack**, and click **Delete**.
- If AWS CLI is installed, run the following command:

  `aws cloudformation delete-stack --stack-name CustomAiCodeGeneratorStack`

